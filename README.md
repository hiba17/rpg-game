# RPG-Game

## General Info
This is a game that uses TTD to:
- Create charcters of type(warrior,Ranger,Mage)
- Create weapons of type (meele,magic,ranged)
- Create Armour of type(Palte,Leahter,cloth) bering bart of either(BODY,LEGS or HEAD).
- Adding and updating weapons and armour to the character and scaling up their respective stats

## Technologies
- IntelliJ
- Java SE
- Gradle
- Design patterns

## Prerequesites
Install java using this link:
````
https://www.oracle.com/java/technologies/javase-downloads.html
````
Install IntelliJ code using this link:
````
https://www.jetbrains.com/idea/download/#section=windows
````
Implement json libaray by going to build.gradle file and typing this under dependencies:
````
    implementation 'org.json:json:20171018'
````
## Features
1. Creates a hero with heroAttributes: Hp,str,int,dex,damage,attack,xpcurrently ,xpnext lvl, and count, with a spesific type

2. Adds item, find out wether it is armour or weapon and sets it in its respective slot(body,legs,head or wepon) , as well as adding the bonus if it is an armour, or damage and attack if it is a weapon

3. creating armour with name and slottype as well as armour level, calulates prosentage, depensing on slot, and has method that calculates bonuses with heroattributes as parameter

3. creating weapon with name and slottype as well as armour level, methods that calulate weapon damage and attack damage dealt.



## Difficulties

I had difficulties trying out strategy pattern as it was the first time.Thing I could to different is not repeating code, as I did in the Items folder, probably with the use of a strategy pattern. As well as better dividing the different methods and classes in the character folder in a more orginized way, to better follow the single resposibility principle.
