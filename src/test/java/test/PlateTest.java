package test;

import items.Armour.Plate;
import items.SlotType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlateTest {

    @Test
    public void  testCreateBODYPlate(){
        SlotType s = SlotType.BODY; //100%
        Plate plate = new Plate("Platasus", s, 2);
        assertEquals(54,plate.getHPB());
        assertEquals(7, plate.getStrB());
        assertEquals(0, plate.getIntB());
        assertEquals(3, plate.getDexB());
        assertEquals(1, plate.getProsentage());
        assertEquals("Platasus",plate.getName());
        assertEquals(SlotType.BODY,plate.getSlot());
        assertEquals(2,plate.getLvl());

    }

    @Test
    public void  testCreateLEGSPlate(){
        SlotType s = SlotType.LEGS; //100%
        Plate plate = new Plate("Platasus", s, 2);
        assertEquals(32,plate.getHPB());
        assertEquals(4, plate.getStrB());
        assertEquals(0, plate.getIntB());
        assertEquals(1, plate.getDexB());
        assertEquals(0.6, plate.getProsentage());

    }
    @Test
    public void  testCreateHEADPlate(){
        SlotType s = SlotType.HEAD; //100%
        Plate plate = new Plate("Platasus", s, 2);
        assertEquals(43,plate.getHPB());
        assertEquals(5, plate.getStrB());
        assertEquals(0, plate.getIntB());
        assertEquals(2, plate.getDexB());
        assertEquals(0.8, plate.getProsentage());

    }

}