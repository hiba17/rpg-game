package test;

import items.Armour.Cloth;
import items.Armour.Leather;
import items.SlotType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LeatherTest {

    @Test
    public void  testCreateBODYLeather(){
        SlotType s = SlotType.BODY; //100%
        Leather leather = new Leather("Leather-Greatness", s, 2);
        assertEquals(36,leather.getHPB());
        assertEquals(3, leather.getStrB());
        assertEquals(0, leather.getIntB());
        assertEquals(7,leather.getDexB());
        assertEquals(1,leather.getProsentage());
        assertEquals("Leather-Greatness",leather.getName());
        assertEquals(SlotType.BODY,leather.getSlot());
        assertEquals(2,leather.getLvl());

    }

    @Test
    public void  testCreateLEGSLeather(){
        SlotType s = SlotType.LEGS; //100%
        Leather leather = new Leather("Yrte", s, 2);
        assertEquals(21,leather.getHPB());
        assertEquals(1, leather.getStrB());
        assertEquals(0, leather.getIntB());
        assertEquals(4,leather.getDexB());
        assertEquals(0.6,leather.getProsentage());

    }
    @Test
    public void  testCreateHEADLeather(){
        SlotType s = SlotType.HEAD; //100%
        Leather leather = new Leather("Cseerifa", s, 2);
        assertEquals(28,leather.getHPB());
        assertEquals(2, leather.getStrB());
        assertEquals(0, leather.getIntB());
        assertEquals(5,leather.getDexB());
        assertEquals(0.8,leather.getProsentage());

    }

}