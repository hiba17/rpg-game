package test;

import character.Slots;
import items.Armour.Cloth;
import items.Armour.Leather;
import items.Armour.Plate;
import items.SlotType;
import items.Weapon.Magic;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SlotsTest {
    @Test
    public void  testAddArmourBODY(){
        Slots slots = new Slots();
        SlotType s = SlotType.BODY; //100%
        Cloth cloth = new Cloth("Brocloth", s, 3);
        slots.addArmour(cloth);

        assertEquals(cloth,slots.getBody());
        assertEquals(null,slots.getLegs());
        assertEquals(null,slots.getHead());
        assertEquals(3,slots.getBody().getLvl());
        assertEquals("Brocloth",slots.getBody().getName());

    }
    @Test
    public void  testAddArmourLEGS(){
        Slots slots = new Slots();
        SlotType s = SlotType.LEGS; //100%
        Leather leather = new Leather("Leatheth", s, 2);
        slots.addArmour(leather);

        assertEquals(null,slots.getBody());
        assertEquals(leather,slots.getLegs());
        assertEquals(null,slots.getHead());
        assertEquals(2,slots.getLegs().getLvl());

    }
    @Test
    public void  testAddArmourHEAD(){
        Slots slots = new Slots();
        SlotType s = SlotType.HEAD; //100%
        Plate plate = new Plate("Platerio", s, 4);
        slots.addArmour(plate);

        assertEquals(null,slots.getBody());
        assertEquals(null,slots.getLegs());
        assertEquals(plate,slots.getHead());
        assertEquals(4,slots.getHead().getLvl());

    }
    @Test
    public void  testAddWeapon(){
        Slots slots = new Slots();
        SlotType s = SlotType.WEAPON; //100%
        Magic magic = new Magic("Magico", s, 4);
        slots.setWeapon(magic);

        assertEquals(magic,slots.getWeapon());
        assertEquals(4,slots.getWeapon().getLvl());

    }

}