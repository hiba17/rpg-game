package test;

import character.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LevelUpXpTest {
    HeroType warrior = HeroType.WARRIOR;
    HeroType mage = HeroType.MAGE;
    HeroType ranger = HeroType.RANGER;
    Hero sandra = new Hero(warrior,new Warrior());
    Hero nora = new Hero(mage,new Mage());
    Hero hiba = new Hero(ranger,new Ranger());
    HeroAttribute s = sandra.getBaseStats();
    HeroAttribute n = nora.getBaseStats();
    HeroAttribute h = hiba.getBaseStats();
    @Test
    public void  testBeforeAddXP(){
        assertEquals(150,s.getHP());
        assertEquals(10, s.getStr());
        assertEquals(3,s.getDex());
        assertEquals(1,s.getInt());
    }
    @Test
    public void  testAddXP(){
        sandra.addXP(100);
        assertEquals(110,s.getXpNext());
        assertEquals(0, s.getXpCurrently());
        assertEquals(110,s.getCount());
        assertEquals(2,s.getLvl());
    }

    @Test
    public void  testAfterAddXP(){
        sandra.addXP(100);
        assertEquals(180,s.getHP());
        assertEquals(15, s.getStr());
        assertEquals(5,s.getDex());
        assertEquals(2,s.getInt());
    }

    @Test
    public void  testAddXPOverMax(){
        nora.addXP(100);
        nora.addXP(300);
        assertEquals(121,n.getXpNext());
        assertEquals(0, n.getXpCurrently());
        assertEquals(121,n.getCount());
        assertEquals(3,n.getLvl());
    }
    @Test
    public void  testAddXPUnderMax(){
        hiba.addXP(100);
        hiba.addXP(300);
        hiba.addXP(20);
        assertEquals(101,h.getXpNext());
        assertEquals(20, h.getXpCurrently());
        assertEquals(121,h.getCount());
        assertEquals(3,h.getLvl());
    }

    //test level upp
    @Test
    public void  testLevelUpRanger(){
        HeroAttribute s = hiba.getBaseStats();
        hiba.getLevelUpXp().levelUp();
        assertEquals(140,s.getHP());
        assertEquals(7, s.getStr());
        assertEquals(15,s.getDex());
        assertEquals(3,s.getInt());
    }

    @Test
    public void  testLevelUpWarrior(){
        sandra.getLevelUpXp().levelUp();
        assertEquals(180,s.getHP());
        assertEquals(15, s.getStr());
        assertEquals(5,s.getDex());
        assertEquals(2,s.getInt());
    }

    @Test
    public void  testLevelUpMage(){
        HeroAttribute s = nora.getBaseStats();
        nora.getLevelUpXp().levelUp();
        assertEquals(115,s.getHP());
        assertEquals(3, s.getStr());
        assertEquals(5,s.getDex());
        assertEquals(15,s.getInt());
    }

}