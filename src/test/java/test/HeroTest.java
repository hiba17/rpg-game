package test;

import character.*;
import items.Armour.Cloth;
import items.Armour.Leather;
import items.Armour.Plate;
import items.SlotType;
import items.Weapon.Meele;
import items.Weapon.Ranged;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    HeroType ranger = HeroType.RANGER;
    Hero hiba = new Hero(ranger,new Ranger());

    //test add-item(weapon, body, legs, head)
    @Test
    public void  testAddItemBODY(){
        SlotType s = SlotType.BODY; //100%
        Cloth cloth = new Cloth("GLogl", s, 3);
        hiba.addItem(cloth);
        HeroAttribute bs = hiba.getBaseStats();
        assertEquals(145,bs.getHP());
        assertEquals(5, bs.getStr());
        assertEquals(14,bs.getDex());
        assertEquals(11,bs.getInt());
        assertEquals(cloth,hiba.getSlots().getBody());
    }

    @Test
    public void  testAddItemLEGSANDBODY(){
        SlotType s = SlotType.BODY; //100%
        Cloth cloth = new Cloth("HEORLE", s, 3);
        hiba.addItem(cloth);
        SlotType sl = SlotType.LEGS; //100%
        Leather leather = new Leather("FOTTERK", sl, 2);
        hiba.addItem(leather);
        HeroAttribute bs = hiba.getBaseStats();
        assertEquals(166,bs.getHP());
        assertEquals(6, bs.getStr());
        assertEquals(18,bs.getDex());
        assertEquals(11,bs.getInt());
        assertEquals(leather,hiba.getSlots().getLegs());
        assertEquals(cloth,hiba.getSlots().getBody());
    }
    @Test
    public void  testAddItemWEAPON(){
        //Before a weapon is added
        HeroAttribute bs = hiba.getBaseStats();
        assertEquals(0,bs.getAttack());
        //after a weapon is added
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("Swordlyra", s, 4);
        hiba.addItem(ranged);
        assertEquals(17,bs.getDamage());
        assertEquals(ranged,hiba.getSlots().getWeapon());
        assertEquals(37,bs.getAttack());
    }

    //test update Item
    @Test
    public void  testUpdateArmour(){
        SlotType sh = SlotType.HEAD; //100%
        Plate plate = new Plate("GLoe-helmet", sh, 2);
        hiba.addItem(plate);
        SlotType s = SlotType.BODY; //100%
        Cloth cloth = new Cloth("POOLER", s, 3);
        SlotType ss = SlotType.BODY; //100%
        hiba.addItem(cloth);
        Leather leather = new Leather("Leather-Beauty", ss, 2);
        hiba.addItem(leather);
        HeroAttribute bs = hiba.getBaseStats();

        assertEquals(199,bs.getHP());
        assertEquals(13, bs.getStr());
        assertEquals(19,bs.getDex());
        assertEquals(2,bs.getInt());
        assertEquals(leather,hiba.getSlots().getBody());
        assertEquals(plate,hiba.getSlots().getHead());
    }

    @Test
    public void  testUpdateWEAPON(){
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("BOW & ARROW", s, 4);
        hiba.addItem(ranged);
        SlotType sw = SlotType.WEAPON; //100%
        Meele meele = new Meele("Slik-Slinger", sw, 4);
        hiba.addItem(meele);
        HeroAttribute bs = hiba.getBaseStats();
        assertEquals(23,bs.getDamage());
        assertEquals(meele,hiba.getSlots().getWeapon());
        assertEquals(30,bs.getAttack());
    }


}