package test;

import character.*;
import items.SlotType;
import items.Weapon.Meele;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MeeleTest {
    @Test
    public void  testCalculateWeaponDamage(){
        SlotType s = SlotType.WEAPON; //100%
        Meele meele = new Meele("Swordlyra", s, 4);
        assertEquals(23,meele.getDamage());
        assertEquals("Swordlyra",meele.getName());
        assertEquals(SlotType.WEAPON,meele.getSlot());
        assertEquals(4,meele.getLvl());
    }

    @Test
    public void  testCalculateAttackRanger(){
        HeroType ranger = HeroType.RANGER;
        Hero hiba = new Hero(ranger,new Ranger());
        HeroAttribute heroAttribute = hiba.getBaseStats();//Zero bonus, hero int= 2
        SlotType s = SlotType.WEAPON; //100%
        Meele meele = new Meele("Swordlyra", s, 4);
        assertEquals(30,meele.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackMage(){
        HeroType mage = HeroType.MAGE;
        Hero hiba = new Hero(mage,new Mage());
        HeroAttribute heroAttribute = hiba.getBaseStats();//Zero bonus, hero int= 2
        SlotType s = SlotType.WEAPON; //100%
        Meele meele = new Meele("Swordlyra", s, 4);
        assertEquals(26,meele.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackWarrior(){
        HeroType warrior = HeroType.WARRIOR;
        Hero hiba = new Hero(warrior,new Warrior());
        HeroAttribute heroAttribute = hiba.getBaseStats();//Zero bonus, hero int= 2
        SlotType s = SlotType.WEAPON; //100%
        Meele meele = new Meele("Swordlyra", s, 4);
        assertEquals(38,meele.calculateAttack(heroAttribute));
    }

}