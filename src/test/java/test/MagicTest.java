package test;

import character.*;
import items.SlotType;
import items.Weapon.Magic;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MagicTest {
    @Test
    public void  testCalculateWeaponDamage(){
        SlotType s = SlotType.WEAPON; //100%
        Magic magic = new Magic("Bloodspell", s, 4);
        assertEquals(33,magic.getDamage());
        assertEquals("Bloodspell",magic.getName());
        assertEquals(SlotType.WEAPON,magic.getSlot());
        assertEquals(4,magic.getLvl());
    }

    @Test
    public void  testCalculateAttackRanger(){
        HeroType ranger = HeroType.RANGER;
        Hero hiba = new Hero(ranger,new Ranger());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Magic magic = new Magic("Expeliamus", s, 4);
        assertEquals(39,magic.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackMage(){
        HeroType mage = HeroType.MAGE;
        Hero hiba = new Hero(mage,new Mage());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Magic magic = new Magic("Levitation", s, 4);
        assertEquals(63,magic.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackWarrior(){
        HeroType warrior = HeroType.WARRIOR;
        Hero hiba = new Hero(warrior,new Warrior());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Magic magic = new Magic("warmoisus", s, 4);
        assertEquals(36,magic.calculateAttack(heroAttribute));
    }


}