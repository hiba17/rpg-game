package test;

import character.*;
import items.SlotType;
import items.Weapon.Ranged;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedTest {
    @Test
    public void  testCalculateWeaponDamage(){
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("RangerAnger", s, 4);
        assertEquals(17,ranged.getDamage());
        assertEquals("RangerAnger",ranged.getName());
        assertEquals(SlotType.WEAPON,ranged.getSlot());
        assertEquals(4,ranged.getLvl());
    }

    @Test
    public void  testCalculateAttackRanger(){
        HeroType ranger = HeroType.RANGER;
        Hero hiba = new Hero(ranger,new Ranger());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("RangerAnger", s, 4);
        assertEquals(37,ranged.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackMage(){
        HeroType mage = HeroType.MAGE;
        Hero hiba = new Hero(mage,new Mage());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("RangerAnger", s, 4);
        assertEquals(23,ranged.calculateAttack(heroAttribute));
    }
    @Test
    public void  testCalculateAttackWarrior(){
        HeroType warrior = HeroType.WARRIOR;
        Hero hiba = new Hero(warrior,new Warrior());
        HeroAttribute heroAttribute = hiba.getBaseStats();
        SlotType s = SlotType.WEAPON; //100%
        Ranged ranged = new Ranged("RangerAnger", s, 4);
        assertEquals(23,ranged.calculateAttack(heroAttribute));
    }
}