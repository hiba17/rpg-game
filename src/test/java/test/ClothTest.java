package test;

import items.Armour.Cloth;
import items.SlotType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClothTest {

    @Test
    public void  testCreateBODYCloth(){
        SlotType s = SlotType.BODY; //100%
        Cloth cloth = new Cloth("GLogl", s, 3);
        assertEquals(25,cloth.getHPB());
        assertEquals(0, cloth.getStrB());
        assertEquals(9,cloth.getIntB());
        assertEquals(4,cloth.getDexB());
        assertEquals(1,cloth.getProsentage());
        assertEquals("GLogl",cloth.getName());
        assertEquals(SlotType.BODY,cloth.getSlot());
        assertEquals(3,cloth.getLvl());

    }

    @Test
    public void  testCreateLEGSCloth(){
        SlotType s = SlotType.LEGS; //100%
        Cloth cloth = new Cloth("Warm Footwear", s, 3);
        assertEquals(15,cloth.getHPB());
        assertEquals(0, cloth.getStrB());
        assertEquals(5,cloth.getIntB()); //5.4
        assertEquals(2,cloth.getDexB());//2,4
        assertEquals(0.6,cloth.getProsentage());

    }
    @Test
    public void  testCreateHEADCloth(){
        SlotType s = SlotType.HEAD; //100%
        Cloth cloth = new Cloth("Bravery Helmet", s, 3);
        assertEquals(20,cloth.getHPB());
        assertEquals(0, cloth.getStrB());
        assertEquals(7,cloth.getIntB()); //5.4
        assertEquals(3,cloth.getDexB());//2,4
        assertEquals(0.8,cloth.getProsentage());

    }

}