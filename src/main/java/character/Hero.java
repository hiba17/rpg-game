package character;

import items.Armour.Armour;
import items.Item;
import items.Weapon.Weapon;

public class Hero {
    private final Slots slots = new Slots();
    private final AddItem addItem= new AddItem();
    private final LevelUpXp levelUpXp;

    private final HeroType type;//WARRIOR; MAGE; RANGER
    private final HeroAttribute baseStats;//Hp,str,int,dex,damage,attack
    private final HeroStrategy Strategy;


    public Hero(HeroType type, HeroStrategy Strategy){
        this.type = type;
        this.Strategy = Strategy;
        baseStats= new HeroAttribute();
        this.levelUpXp= new LevelUpXp(baseStats,Strategy);
        getBase();

    }
    //Method to add experience points,add it to the class levelupxp, that delas with
    //calulating newxp and setting all the xp values as well as scaling up the basestats when we level up
    public void addXP(int newXP){
        levelUpXp.addXP(newXP);
    }
    //method addItem
    public void addItem(Item item ) {
        //check if item is of abstract class armour or weapon
        if (item instanceof Armour) {
            //if yes then add the armours bonus to the basestats
            //this is done in the class addItem that adds damage,attack and adds/removes armour-bonuses
            addItem.addBonus( (Armour) item,baseStats);
            //then we add armour to slots object and return the past item that was there
            Armour pastItem = slots.addArmour((Armour) item);
            //If there was a past item there , we remove their bonuses from basestats
            if (pastItem != null) {
                addItem.removeBonus(pastItem,baseStats);
            }
        } //if item is a weapon
        else{
            // we add the damage and attack values from the weapon to the basestats.damage and attack variables
            addItem.addDamageAttack(baseStats,(Weapon) item);
            //Set the weapon in the slot
            slots.setWeapon((Weapon) item);
        }
    }

    //Method to sett the base stats to their perspective values when we first make a hero object
    private void getBase(){ Strategy.getBase(baseStats); }
    //method to get the basestats
    public HeroAttribute getBaseStats() { return baseStats; }
    //get methods
    public Slots getSlots() { return slots; }
    public LevelUpXp getLevelUpXp() {
        return levelUpXp;
    }





















}
