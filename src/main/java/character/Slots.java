package character;

import items.Armour.Armour;
import items.SlotType;
import items.Weapon.Weapon;


public class Slots {
    Weapon weapon = null;
    Armour body   = null;
    Armour head   = null;
    Armour legs   = null;

    //method that finds out what slotType it is and sets it to the corresponding variable
    public Armour addArmour(Armour item){
        Armour pastItem = null;
        if(item.getSlot()== SlotType.BODY){
            pastItem=body;
           body = item;
        }
        else if(item.getSlot()== SlotType.LEGS){
            pastItem=legs;
            legs = item;
        }
        else if(item.getSlot()== SlotType.HEAD){
            pastItem=head;
            head = item;
        }
        //sets the pastItem to whatever was held on ,and return it so it's bonuses can be removed
        //from the baseStats
        return pastItem;
    }

    //Getters and setter methods
    public Armour getBody() {
        return body;
    }
    public Armour getHead() {
        return head;
    }
    public Armour getLegs() {
        return legs;
    }
    public Weapon getWeapon() {
        return weapon;
    }
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }






}
