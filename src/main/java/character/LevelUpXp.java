package character;

public class LevelUpXp {
    HeroAttribute baseStats;
    HeroStrategy Strategy;
    public LevelUpXp(HeroAttribute baseStats,HeroStrategy Strategy){
        this.baseStats=baseStats;
        this.Strategy=Strategy;
    }

    public void addXP(int newXP){
        //if the new points are bigger that what needs to reach next level
        //discard rest of the points
        if(newXP>baseStats.getXpNext()){
            baseStats.setXpCurrently(baseStats.getXpCurrently() + baseStats.getXpNext());
            baseStats.setXpNext(0);
        }
        //if not then use the points to add to xp we currently have and remove it from next
        else{
            baseStats.setXpCurrently(baseStats.getXpCurrently() + newXP);
            baseStats.setXpNext(baseStats.getXpNext() - newXP);
        }
        //After every increase in xp we update. meaning we see if the amout of xp is reached to move
        //up a level
        updateLevel();
    }
    //method updateLevel (updates nr level)
    public void updateLevel(){
        //if xp is reached for next level, add lvl by one, and we calculate a newxp and add
        //the bonuses from the leveling to the basestats
        if(baseStats.getXpNext()==0){
            baseStats.setLvl(baseStats.getLvl()+1);
            calculateNewXP();
            levelUp();
        }
    }
    //method that takes the current xplevel, and increases it by 10% after each leveling up
    public void calculateNewXP(){
        int i = (int) (baseStats.getCount()+ ((10*baseStats.getCount())/100));
        //xpcurrently gets sat to 0 again and xpnext and count get sat to next xpvalue to reach
        baseStats.setXpNext(i);
        baseStats.setCount(i);
        baseStats.setXpCurrently(0);
    }
    //This method add the bonuses that leveling up gives the basestats for each given heroType
    public void levelUp(){
        Strategy.levelUp(baseStats);
    }
    //getMethod
    public int getLvl() {
        return baseStats.getLvl();
    }
}
