package character;


public class Warrior implements HeroStrategy {
    //when Warrior-object is created these numbers are added the base stats
    @Override
    public void getBase(HeroAttribute baseStats) {
        baseStats.Dex = 3;
        baseStats.Int = 1;
        baseStats.Str = 10;
        baseStats.HP  = 150;
    }
    //when warrior is leveling up these numbers are added to the basestats
    @Override
    public void levelUp(HeroAttribute baseStats) {
        baseStats.Dex += 2;
        baseStats.Int += 1;
        baseStats.Str += 5;
        baseStats.HP  += 30;

    }

}
