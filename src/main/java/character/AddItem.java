package character;

import items.Armour.Armour;
import items.Weapon.Weapon;

public class AddItem {

    //Method to remove past items bonuses, aka update basestats value
    public void removeBonus(Armour armour,HeroAttribute baseStats){
        baseStats.setStr(baseStats.getStr() - armour.getStrB());
        baseStats.setInt(baseStats.getInt() - armour.getIntB());
        baseStats.setDex(baseStats.getDex() - armour.getDexB());
        baseStats.setHP (baseStats.getHP()  - armour.getHPB());
    }
    //method to add the bonuses from the armour to the basestats
    public void addBonus(Armour Armour,HeroAttribute baseStats ){
        baseStats.HP  += Armour.getHPB();
        baseStats.Dex += Armour.getDexB();
        baseStats.Int += Armour.getIntB();
        baseStats.Str += Armour.getStrB();

    }
    //Method to add damage and attack dealt from the weapon added
    public void addDamageAttack(HeroAttribute baseStats, Weapon weapon ){
        baseStats.damage = weapon.getDamage();
        baseStats.attack = weapon.calculateAttack(baseStats);
    }
}
