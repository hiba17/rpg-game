package character;

public class Mage implements HeroStrategy {
    //when Mage-object is created these numbers are added the base stats
    @Override
    public void getBase(HeroAttribute baseStats) {
        baseStats.Dex = 3;
        baseStats.Int = 10;
        baseStats.Str = 2;
        baseStats.HP  = 100;
    }
    //when Mage is leveling up these numbers are added to the basestats
    @Override
    public void levelUp(HeroAttribute baseStats) {
        baseStats.Dex += 2;
        baseStats.Int += 5;
        baseStats.Str += 1;
        baseStats.HP  += 15;

    }

}
