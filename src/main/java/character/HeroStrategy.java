package character;

public interface HeroStrategy {
    //Method that  sets hp,Str,Int and Dex different to each Herotype
    void getBase(HeroAttribute baseStats);
    //Method that scales hp,Str,Int and Dex with the values different to each Herotype
    void levelUp(HeroAttribute baseStats);

}
