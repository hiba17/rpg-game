package character;


public class Ranger implements HeroStrategy {
    //when ranger-object is created these numbers are added the base stats
    @Override
    public void getBase(HeroAttribute baseStats) {
        baseStats.Dex = 10;
        baseStats.Int = 2;
        baseStats.Str = 5;
        baseStats.HP  = 120;
    }
    //when ranger is leveling these numbers are added to the basestats
    @Override
    public void levelUp(HeroAttribute baseStats) {
        baseStats.Dex += 5;
        baseStats.Int += 1;
        baseStats.Str += 2;
        baseStats.HP  += 20;

    }


}
