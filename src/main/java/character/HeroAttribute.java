package character;

public class HeroAttribute {
    protected int HP,Str,Dex,Int;
    protected int damage,attack;
    protected int xpCurrently=0;
    protected int xpNext=100;
    protected int count =100;
    protected int Lvl=1;

    //getters and setters for the variables
    public int getXpCurrently() {
        return xpCurrently;
    }
    public void setXpCurrently(int xpCurrently) {
        this.xpCurrently = xpCurrently;
    }
    public int getXpNext() {
        return xpNext;
    }
    public void setXpNext(int xpNext) {
        this.xpNext = xpNext;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public int getLvl() {
        return Lvl;
    }
    public void setLvl(int lvl) {
        Lvl = lvl;
    }

    public int getDamage() {
        return damage;
    }
    public int getAttack() {
        return attack;
    }
    public int getHP() {
        return HP;
    }
    public void setHP(int HP) {
        this.HP = HP;
    }
    public int getStr() {
        return Str;
    }
    public void setStr(int str) {
        Str = str;
    }
    public int getDex() {
        return Dex;
    }
    public void setDex(int dex) {
        Dex = dex;
    }
    public int getInt() {
        return Int;
    }
    public void setInt(int anInt) {
        Int = anInt;
    }


}
