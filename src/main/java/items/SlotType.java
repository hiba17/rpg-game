package items;

public enum SlotType {
    BODY,
    LEGS,
    WEAPON,
    HEAD
}
