package items.Armour;

import items.SlotType;

public class Plate extends Armour {
    public Plate(String name, SlotType slot, int Lvl) {
        super(name, slot,Lvl);
        //using method under and adding the results to their respective values
        setHPB(addBonus(30,12));
        setStrB(addBonus(3,2));
        setDexB(addBonus(1,1));
        setIntB(0);
    }

    //sett the bonuses to add them to hero later
    private int addBonus( int base,int scale){
        double nr = (base+(scale*Lvl))*prosentage;
        return (int) nr;
    }

}
