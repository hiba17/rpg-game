package items.Armour;

import items.SlotType;

public class Cloth extends Armour {

    public Cloth(String name, SlotType slot, int Lvl) {
        //using method under and adding the results to their respective values
        super(name, slot,Lvl);
        setHPB(addBonus(10,5));
        setIntB(addBonus(3,2));
        setDexB(addBonus(1,1));
        setStrB(0);

    }


    //sett the bonuses to add them to hero later
    private int addBonus( int base,int scale){
        double nr = (base+(scale*Lvl))*prosentage;
        return (int) nr;
    }



}
