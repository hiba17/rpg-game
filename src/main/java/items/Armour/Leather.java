package items.Armour;


import items.SlotType;

public class Leather extends Armour {
    public Leather(String name, SlotType slot, int Lvl) {
        super(name, slot,Lvl);
        //using method under and adding the results to their respective values
        setHPB(addBonus(20,8));
        setStrB(addBonus(1,1));
        setDexB(addBonus(3,2));
        setIntB(0);
    }
    //sett the bonuses to add them to hero later
    private int addBonus( int base,int scale){
        double nr = (base+(scale*Lvl))*prosentage;
        return (int) nr;
    }

}
