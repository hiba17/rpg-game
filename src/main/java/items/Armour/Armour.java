package items.Armour;

import items.Item;
import items.SlotType;

public abstract class Armour extends Item {
    int HPB;
    int IntB;
    int StrB;
    int DexB;
    double prosentage;


    public Armour(String name, SlotType slot, int lvl) {
        super(name, slot, lvl);
        setProsentage();
    }
    //when an armour object is created (either type) we set the prosentage we nedd to colulate bonuses
    //dealt  by finding the slotType
    public void setProsentage(){
        if(slot == SlotType.BODY){
           prosentage=1;
        }
        else if(slot== SlotType.LEGS){
            prosentage=0.6;
        }
        else{
            prosentage=0.8;
        }
    }

    //Getters and setters methods for the variables above
    public int getHPB() {
        return HPB;
    }
    public void setHPB(int HPB) {
        this.HPB = HPB;
    }
    public int getIntB() {
        return IntB;
    }
    public void setIntB(int intB) {
        IntB = intB;
    }
    public int getStrB() {
        return StrB;
    }
    public void setStrB(int strB) {
        StrB = strB;
    }
    public int getDexB() {
        return DexB;
    }
    public void setDexB(int dexB) {
        DexB = dexB;
    }
    public double getProsentage() {
        return prosentage;
    }



}
