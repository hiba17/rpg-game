package items.Weapon;

import character.HeroAttribute;
import items.SlotType;

public class Ranged extends Weapon {


    public Ranged(String name, SlotType slot, int lvl) {
        //when Ranged-object is created we set base damage and calculate weapon-damage
        super(name, slot,lvl);
        this.setBaseDamage(5);
        calculateWeaponDamage();
    }
    //method sets damage  to the basedamage plus scaling of level
    private void calculateWeaponDamage( ){
        damage = (int) (baseDamage+(getLvl()*3));
    }
    //overriding weapon method with calculating attack with the hero dex value(rounding down)
    @Override
    public int  calculateAttack(HeroAttribute hero){
        attack = (int) (damage + ((hero.getDex())*2));
        return attack;
    }
}
