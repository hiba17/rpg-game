package items.Weapon;

import character.HeroAttribute;
import items.Item;
import items.SlotType;

public abstract class Weapon extends Item {
    int baseDamage;
    int damage;
    int attack;

    public Weapon(String name, SlotType slot, int lvl) {
        super(name, slot,lvl);
    }

    public int getDamage() {
        return damage;
    }

    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }

    public int  calculateAttack(HeroAttribute hero){
        return attack;
    }





}
