package items.Weapon;

import character.HeroAttribute;
import items.SlotType;

public class Meele extends Weapon {

    public Meele(String name, SlotType slot, int lvl){
        //when Meele-object is created we set base damage and calculate weapon-damage
        super(name,slot,lvl);
        this.setBaseDamage(15);
        calculateWeaponDamage();
    }
    //method sets damage  to the basedamage plus scaling of level
    private void calculateWeaponDamage(){
        damage = (int) ( baseDamage+(getLvl()*2));
    }
    //overriding weapon method with calculating attack with the hero str value(rounding down)
    @Override
    public int  calculateAttack(HeroAttribute hero){
        attack = (int) (damage + ((hero.getStr())*1.5));
        return attack;
    }


}
