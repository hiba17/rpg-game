package items.Weapon;

import character.HeroAttribute;
import items.SlotType;

public class Magic extends Weapon {
    //when Magic-object is created we set base damage and calculate weapon-damage
    public Magic(String name, SlotType slot, int lvl) {
        super(name, slot,lvl);
        this.setBaseDamage(25);
        calculateWeaponDamage();
    }
    //method sets damage  to the basedamage plus scaling of level
    private void calculateWeaponDamage(){
        damage = (int) baseDamage+(getLvl()*2);
    }

    //overriding weapon method with calculating attack with the hero int value(rounding down)
    @Override
    public int  calculateAttack(HeroAttribute hero){
        attack = (int) (damage + ((hero.getInt())*3));
        return attack;
    }
}
