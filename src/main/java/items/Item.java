package items;

public abstract class Item {
   protected int Lvl;
   private String name;
   protected SlotType slot;

    public Item(String name, SlotType slot, int lvl){
        this.name=name;
        this.slot=slot;
        Lvl=lvl;
    }
    //get methods
    public SlotType getSlot() {
        return slot;
    }
    public int getLvl() {
        return Lvl;
    }
    public String getName() {
        return name;
    }


}
